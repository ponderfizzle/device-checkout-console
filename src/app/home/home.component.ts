import { Component, OnInit } from '@angular/core';
import { FirebaseConfigService } from '../core/services/firebase-config.service';
import { NgForm, FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Device } from '../core/interfaces/Device';

import { DeviceDetailModalComponent } from '../modals/device-detail-modal/device-detail-modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private firebaseConfigService: FirebaseConfigService) { }

  deviceInfo: Array<Device> = [];
  sortedDeviceInfo: Array<Device> = [];
  pagedDeviceInfo: Array<Device> = [];
  currentSortType: string = "none";  // valid values: 'name', 'osversion', 'ostype', 'borrower';
  currentSortDirection: string = "none"; // valid values: 'none', 'asc', 'desc';
  differentColumnClicked: boolean = false;

  ngOnInit() {
    this.firebaseConfigService.getAllDevices().subscribe(devices => {
      this.deviceInfo = devices;
      this.currentSortDirection = 'none';
    })
  }

  openModal(deviceInfo) { }

  searchValue(deviceText, searchText) {
    //  solution below is from https://stackoverflow.com/questions/20881213/converting-json-object-into-javascript-array
    // var textSearchArray = Object.keys(deviceText).map(function (item) { return deviceText[item] })
    var textSearchArray: Array<string> = Object.keys(deviceText).map(function (item) { return deviceText[item] })
    var searchString: string = textSearchArray.join();
    // This may be better implemented as a Pipe at a later date but this works and isn't very complicated
    if (!searchText) {
      return true;
    }
    else {
      return searchString.toLowerCase().includes(searchText.toLowerCase());
    }
  }

  sortDeviceTable(columnName: string) {
    if (columnName !== this.currentSortType) {
      this.differentColumnClicked = false;
    }
    else {
      this.differentColumnClicked = true;
    }

    this.currentSortType = columnName;
    this.sortedDeviceInfo = this.deviceInfo.slice(0);

    if (this.currentSortType == 'osversion') {
        // split the version number and convert to an integer
      for (let i = 0; i < this.sortedDeviceInfo.length; i++) {
        this.sortedDeviceInfo[i][columnName] = this.sortedDeviceInfo[i][columnName].split('.');
      }
      this.sortedDeviceInfo.sort(function (a, b) {
        var x = a[columnName];
        var y = b[columnName];

        if (x[0] === y[0]) {
          let i = 0;
          var compareX = x[i];
          var compareY = y[i];

          while (compareX == compareY) {
            if (x[i] === undefined && y[i] === undefined) {
              break;
            }

            if (x[i] === undefined) {
              compareX = 0;
            }
            else {
              compareX = x[i];
            }

            if (y[i] === undefined) {
              compareY = 0;
            }
            else {
              compareY = y[i];
            }
            i += 1;
          }
          return compareX - compareY;
        }

        if (x[0] != y[0]) {
          return x[0] - y[0];
        }
      });

      for (let i = 0; i < this.sortedDeviceInfo.length; i++) {
        this.sortedDeviceInfo[i][columnName] = this.sortedDeviceInfo[i][columnName].join('.');
      }
    }
    else {
      this.sortedDeviceInfo.sort(function (a, b) {
        var x = a[columnName].toLowerCase();
        var y = b[columnName].toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
      });
    }

    if (this.differentColumnClicked === false) {
      this.currentSortDirection = 'desc';
      this.deviceInfo = this.sortedDeviceInfo;
    }
    else if (this.differentColumnClicked && this.currentSortDirection !== 'desc') {
      this.currentSortDirection = 'desc';
      this.deviceInfo = this.sortedDeviceInfo;
    }
    else {
      this.currentSortDirection = 'asc';
      this.deviceInfo = this.sortedDeviceInfo.reverse();
    }
    
    // sort comes from https://stackoverflow.com/questions/1069666/sorting-javascript-object-by-property-value
  }
}
