import { Component, OnInit } from '@angular/core';
import { FirebaseConfigService } from '../core/services/firebase-config.service';
import { FormsModule, NgForm, PatternValidator } from '@angular/forms';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.css']
})
export class AddDeviceComponent implements OnInit {
  name: string;
  assetNumber: number;
  deviceType: string;
  owner: string;
  osType: string;
  osVersion: string;
  provider: string = "";
  note: string = "";
  checkouts: number = 0;
  action: string = "added";


  constructor(private firebaseConfigService: FirebaseConfigService) { }

  ngOnInit() { }

  addDevice() {
    this.firebaseConfigService.addDevice(this.name, this.assetNumber, this.deviceType, this.owner, this.osType, this.osVersion, this.checkouts, this.action, this.provider, this.note)
  }

}
