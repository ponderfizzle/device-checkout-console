import { Injectable } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { FIREBASE_CONFIG } from '../constants/constants';
import { Device } from '../interfaces/device';
import { Alert } from '../interfaces/alert';
import { History } from '../interfaces/history';

@Injectable()
export class FirebaseConfigService {
    devices: FirebaseListObservable<Device[]>;
    popularDevices: FirebaseListObservable<Device[]>;
    history: FirebaseListObservable<History[]>;
    alert: FirebaseListObservable<Alert[]>;

    constructor(private db: AngularFireDatabase) { }


    addDevice(name: string, assetNumber: number, deviceType: string, owner: string, osType: string, osVersion: string, checkouts: number, action: string, provider?: string, note?: string) {
        var postData: Device = {
            name: name,
            assetnumber: assetNumber,
            devicetype: deviceType,
            owner: owner,
            ostype: osType,
            osversion: osVersion,
            provider: provider,
            note: note,
            borrower: '',
            lastmodified: Date.now(),
            checkouts: checkouts,
            action: action
        }

        var history: History = {
            name: name,
            action: action,
            lastmodified: Date.now()
        }

        this.db.database.ref('/devices/').push(postData);
        this.db.database.ref('/history/').push(history);
    }

    // Returns all devices in the database as an Observable
    getAllDevices() {
        return this.devices = this.db.list('/devices/', {
            query: {
                orderByChild: 'ostype'
            }
        }) as FirebaseListObservable<Device[]>
    }

    // Devices have a unique ID set by Firebase along with the unique asset # given by the IS dept.
    updateDevice(id: string, assetNumber: string, name: string, deviceType: string, owner: string, osType: string, osVersion: string, borrower: string, checkouts: number, provider?: string, note?: string) {
        // an error will occur if trying to set a property that doesn't already exist
        if (!provider) {
            provider = "";
        }
        if (!note) {
            note = "";
        }
        const deviceToUpdate = this.db.object('/devices/' + id);
        deviceToUpdate.set({ assetnumber: assetNumber, name: name, devicetype: deviceType, owner: owner, ostype: osType, osversion: osVersion, borrower: borrower, checkouts: checkouts, provider: provider, note: note, lastmodified: Date.now() })
    }

    deleteDevice(id: string) {
        const deviceToDelete = this.db.list('/devices/' + id);
        deviceToDelete.remove();
    }

    getPopularDevices() {
        return this.popularDevices = this.db.list('/devices/', {
            query: {
                orderByChild: 'checkouts',
                limitToLast: 5
            }
        }) as FirebaseListObservable<Device[]>
    }

    getHistory() {
        return this.history = this.db.list('/history/', {
            query: {
                orderByChild: 'lastmodified',
                limitToLast: 200 // limits history to 200 items - can be increased or decreased to suit needs
            }
        }) as FirebaseListObservable<History[]>
    }

    getAlerts() {
        return this.alert = this.db.list('/alerts/', {
        }) as FirebaseListObservable<Alert[]>
    }

    updateAlertFlag(alertFlag: boolean) {
        const alertList: FirebaseObjectObservable<Alert> = this.db.object('/alerts/');
        alertList.update({ alertflag: alertFlag });
    }

    updateAlertMessage(alertMessage: string) {
        const alertList: FirebaseObjectObservable<Alert> = this.db.object('/alerts/');
        alertList.update({ alertmessage: alertMessage });
    }

}