export interface Device {
    $key?: string,
    assetnumber?: number,
    borrower?: string,
    devicetype?: string,
    lastmodified?: number,
    name?: string,
    note?: string,
    ostype?: string,
    action?: string,
    osversion?: string,
    provider?: string,
    owner?: string,
    checkouts?: number
}
