export interface History {
    $key?: string,
    name: string,
    action?: string,
    borrower?: string,
    lastmodified?: number
}
