import { Component, OnInit } from '@angular/core';
import { FirebaseConfigService } from '../../core/services/firebase-config.service';

@Component({
  selector: 'app-alert-message',
  templateUrl: './alert-message.component.html',
  styleUrls: ['./alert-message.component.css']
})
export class AlertMessageComponent implements OnInit {

  alertFlag: boolean;
  alertMessage: string;

  constructor(private firebaseConfigService: FirebaseConfigService) { }

  ngOnInit() {
    this.firebaseConfigService.getAlerts().subscribe(alerts => {
      this.alertFlag = alerts[0]['$value'];
      this.alertMessage = alerts[1]['$value'];
})
  }

}
