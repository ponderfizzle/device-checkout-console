import { Component, OnInit } from '@angular/core';
import { FirebaseConfigService } from '../../core/services/firebase-config.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-alert-manager',
  templateUrl: './alert-manager.component.html',
  styleUrls: ['./alert-manager.component.css']
})
export class AlertManagerComponent implements OnInit {

  alertFlag: boolean;
  alertMessage: string;
  newAlertMessage: string;
  alertSubscription: Subscription;


  constructor(private firebaseConfigService: FirebaseConfigService) { }

  ngOnInit() {
    this.alertSubscription = this.firebaseConfigService.getAlerts().subscribe(alerts => {
      this.alertFlag = alerts[0]['$value'];
      this.alertMessage = alerts[1]['$value'];
    })
  }

  ngOnDestroy() {
    this.alertSubscription.unsubscribe();
  }

  updateAlertFlag(flag) {
    this.firebaseConfigService.updateAlertFlag(flag);
  }

  updateAlertMessage(message) {
    this.firebaseConfigService.updateAlertMessage(message);
    this.newAlertMessage = "";
    // The maximum size of a child value is 10mb (UTF-8 encoded) but I limit input to 500 characters to prevent a message being uneccessarily long
  }
}
