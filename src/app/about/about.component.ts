import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  deviceToggle: boolean = true;
  checkoutToggle: boolean = true;

  isCollapsed(clickedTarget) {
    if (clickedTarget == 'deviceInfo' && this.deviceToggle) {
      this.deviceToggle = false;
    }
    else if (clickedTarget == 'deviceInfo' && !this.deviceToggle) {
      this.deviceToggle = true;
    }

    if (clickedTarget == 'checkoutInfo' && this.checkoutToggle) {
      this.checkoutToggle = false;
    }
    else if (clickedTarget == 'checkoutInfo' && !this.checkoutToggle) {
      this.checkoutToggle = true;
    }
  }

}
