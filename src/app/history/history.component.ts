import { Component, OnInit } from '@angular/core';
import { FirebaseConfigService } from '../core/services/firebase-config.service';
import { Subscription } from 'rxjs/Subscription';
import { Device } from '../core/interfaces/Device';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  historyInfo: Array<Device> = [];
  popularDevices: Array<Device> = [];
  historySubscription: Subscription;
  popularDeviceSubscription: Subscription;

  constructor(private firebaseConfigService: FirebaseConfigService) { }

  ngOnInit() {
    this.historySubscription = this.firebaseConfigService.getHistory().subscribe(history => {
      this.historyInfo = history;
      this.historyInfo.sort(function (a, b) {
        var x = a['lastmodified'];
        var y = b['lastmodified'];
        return x < y ? 1 : x > y ? -1 : 0;
      });
      // The number of history items returned can be configured in firebase-config.service.ts (default is 200)   
    })


    this.popularDeviceSubscription = this.firebaseConfigService.getPopularDevices().subscribe(popularDevices => {
      this.popularDevices = popularDevices;
      this.popularDevices.sort(function (a, b) {
        var x = a['checkouts'];
        var y = b['checkouts'];
        return x < y ? 1 : x > y ? -1 : 0;
      });
      // The number of returned devices can be configured in firebase-config.service.ts (default is 5)   
    })
  }

  ngOnDestroy() {
    this.popularDeviceSubscription.unsubscribe();
    this.historySubscription.unsubscribe();
  }

}
