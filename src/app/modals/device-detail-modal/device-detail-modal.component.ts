import { Component, OnInit } from '@angular/core';
import { FirebaseConfigService } from '../../core/services/firebase-config.service';
import { Device } from '../../core/interfaces/Device';
import { Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'device-detail',
  templateUrl: './device-detail-modal.component.html',
  styleUrls: ['./device-detail-modal.component.css'],
  animations: [
    trigger('flyInOut', [
      state('in', style({transform: 'translateY(0)'})),
      transition('void => *', [
        style({transform: 'translateY(100%)'}),//
        animate('250ms ease-in')
      ]),
      transition('* => void', [
        animate('250ms ease-out', style({transform: 'translateY(100%)'}))
      ])
    ])
  ]
})

export class DeviceDetailModalComponent implements OnInit {
  $key: string;
  assetNumber: string;
  borrower: string;
  deviceType: string;
  lastModified: number;
  name: string;
  note: string;
  osType: string;
  provider: string;
  osVersion: string;
  owner: string;
  checkouts: number;
  currentDevice: Device;
  confirmationModalSwitch: boolean = false;

  constructor(private firebaseConfigService: FirebaseConfigService) { }

  ngOnInit() { }

  fillFormDetail(deviceDetail) {
    console.log(deviceDetail);
    this.currentDevice = deviceDetail;
    this.$key = deviceDetail.$key;
    this.assetNumber = deviceDetail.assetnumber;
    this.borrower = deviceDetail.borrower;
    this.deviceType = deviceDetail.devicetype;
    this.lastModified = deviceDetail.lastmodified;
    this.name = deviceDetail.name;
    this.provider = deviceDetail.provider;
    this.note = deviceDetail.note;
    this.osType = deviceDetail.ostype;
    this.osVersion = deviceDetail.osversion;
    this.owner = deviceDetail.owner;
    this.checkouts = deviceDetail.checkouts;
  }

  updateDevice() {
    this.firebaseConfigService.updateDevice(this.$key, this.assetNumber, this.name, this.deviceType, this.owner, this.osType, this.osVersion, this.borrower, this.checkouts, this.provider, this.note);
  }

  deleteDevice() {
    this.firebaseConfigService.deleteDevice(this.$key);
    this.confirmationModalSwitch = false; //set the switch to false or else the confirmation dialog will display after a device has been deleted
  }

  deleteInProgress(action) {
    // console.log("switch is: " + this.confirmationModalSwitch);
    this.confirmationModalSwitch = action;
  }


}
