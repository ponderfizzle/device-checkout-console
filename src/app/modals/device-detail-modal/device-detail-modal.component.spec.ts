import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceDetailModalComponent } from './device-detail-modal.component';

describe('ModalComponent', () => {
  let component: DeviceDetailModalComponent;
  let fixture: ComponentFixture<DeviceDetailModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceDetailModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceDetailModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
