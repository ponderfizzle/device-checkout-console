// Module configurations
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, NgForm } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Firebase and AngularFire2 Configurations
import { AngularFireModule } from 'angularfire2';
import { FirebaseConfigService } from './core/services/firebase-config.service';
import { FIREBASE_CONFIG } from './core/constants/constants';
import { AngularFireDatabaseModule } from 'angularfire2/database';

// Component configurations
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { HistoryComponent } from './history/history.component';
import { AddDeviceComponent } from './add-device/add-device.component';
import { DeviceDetailModalComponent } from './modals/device-detail-modal/device-detail-modal.component';
import { AlertMessageComponent } from './alerts/alert-message/alert-message.component';
import { AlertManagerComponent } from './alerts/alert-manager/alert-manager.component';


const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'add-device', component: AddDeviceComponent },
  { path: 'app-alert-manager', component: AlertManagerComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AboutComponent,
    HistoryComponent,
    AddDeviceComponent,
    DeviceDetailModalComponent,
    AlertMessageComponent,
    AlertManagerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    AngularFireModule.initializeApp(FIREBASE_CONFIG.firebase),
    AngularFireDatabaseModule,
    BrowserAnimationsModule
  ],
  providers: [FirebaseConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
