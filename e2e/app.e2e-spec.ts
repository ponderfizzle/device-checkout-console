import { GameListPage } from './app.po';

describe('game-list App', () => {
  let page: GameListPage;

  beforeEach(() => {
    page = new GameListPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
