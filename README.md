## device-checkout-console
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.6.

## Constants File
Connecting to the Firebase databse requires a constants folder/file with the appropriate connection information (src/app/core/constants/constants.ts

## Installing in IIS
The following steps describe how to install and use the application using IIS
  1.  Run 'ng build' to build the app
  2.  Copy the files from the `dist/` directory to the `C:\inetpub\wwwroot` directory
  3.  Open the index.html file with a text editor such as Notepadd++
  4.  Comment the line containing `<base href="/">`
  5.  Uncomment the line containing `<base href="/devicecheckout/">`
  6.  Save changes that were made.
  7.  The app should now function correctly when run as an IIS application.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.